const jwt = require('jsonwebtoken');
const SECRET = process.env.JWT_SECRET;

const createToken = (data) => jwt.sign(data, SECRET);
const decodToken = (data) => jwt.verify(data, SECRET);

module.exports = {createToken, decodToken};
