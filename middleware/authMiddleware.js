const bcrypt = require('bcrypt');
const {User} = require('../models/userSchema');
const {createToken, decodToken} = require('../helpers/jwtHelper');

const checkUserName = async (req, res, next) => {
  const {username} = req.body;
  const user = await User.findOne({username});

  if (user) {
    return res.status(400).json({
      message: 'User is exist',
    });
  }

  next();
};

const registerUser = async (req, res) => {
  const {username, password} = req.body;

  const user = new User({
    username: username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  return res.status(200).json({
    message: 'Success',
  });
};

const loginUser = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});
  const checkPassword = user ?
    await bcrypt.compare(password, user.password) :
    false;
  const userToken = user ? createToken({id: user._id}) : false;

  if (!user) {
    return res.status(400).json({
      message: 'User not found',
    });
  }

  if (!checkPassword) {
    return res.status(400).json({
      message: 'Wrong password',
    });
  }

  return res.status(200).json({
    message: 'Success',
    jwt_token: userToken,
  });
};

const authorization = async (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({
      message: 'No authorization',
    });
  }

  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    return res.status(400).json({
      message: 'No token',
    });
  }

  const userData = decodToken(token);
  req.body = {
    ...req.body,
    id: userData.id,
  };
  next();
};

const getUserData = async (req, res) => {
  const {id} = req.body;
  const {_id, username, createdDate} = await User.findOne({_id: id});

  return res.status(200).json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
};

const changePassword = async (req, res) => {
  const {id, oldPassword, newPassword} = req.body;

  if (oldPassword === newPassword) {
    return res.status(400).json({
      message: 'New password must be defferrnt from old',
    });
  }

  const user = await User.findOne({_id: id});
  const checkPassword = await bcrypt.compare(oldPassword, user.password);

  if (!checkPassword) {
    return res.status(400).json({
      message: 'Wrong password',
    });
  }
  
  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});

  return res.status(200).json({
    message: 'Success',
  });
};

const deleteUser = async (req, res) => {
  const {id} = req.body;
  await User.findByIdAndDelete({_id: id});

  return res.status(200).json({
    message: 'Success',
  });
};

module.exports = {
  checkUserName,
  registerUser,
  loginUser,
  authorization,
  getUserData,
  changePassword,
  deleteUser,
};
