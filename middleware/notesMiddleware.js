const {Note} = require('../models/notesSchema');

const saveNote = async (req, res) => {
  const {id, text} = req.body;
  const note = new Note({
    userId: id,
    text: text,
  });

  await note.save();

  return res.status(200).json({
    message: 'Success',
  });
};

const getNoteById = async (req, res) => {
  const {id} = req.body;
  const noteId = req.params.id;

  const note = await Note.findOne({_id: noteId, userId: id});

  if (!note) {
    return res.status(400).json({
      message: 'Note not finde',
    });
  }
  const {_id, userId, completed, text, createdDate} = note;

  res.status(200).send({
    note: {
      _id,
      userId,
      completed,
      text,
      createdDate,
    },
  });
};

const checkNoteById = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId});

  if (!note) {
    return res.status(400).json({
      message: 'Note not finde',
    });
  }
  const {completed} = note;
  await note.updateOne({completed: !completed});

  return res.status(200).send({
    message: 'Success',
  });
};

const updateNoteById = async (req, res) => {
  const {id, text} = req.body;
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId, userId: id});

  if (!note) {
    return res.status(400).json({
      message: 'Note not finde',
    });
  }

  await note.updateOne({text});

  return res.status(200).send({
    message: 'Success',
  });
};

const deleteNoteById = async (req, res) => {
  const {id} = req.body;
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId, userId: id});

  if (!note) {
    return res.status(400).json({
      message: 'Note not finde',
    });
  }

  await Note.findOneAndDelete({_id: noteId, userId: id});

  return res.status(200).send({
    message: 'Success',
  });
};

const getAllUsersNote = async (req, res) => {
  const {id} = req.body;
  const limit = req.query.limit || 0;
  const offset = req.query.offset || 0;

  const records = await Note.find({userId: {$in: id}})
      .select({
        _id: 1,
        userId: 1,
        completed: 1,
        text: 1,
        createdDate: 1,
      })
      .skip(+offset)
      .limit(+limit);

  return res.status(200).send({
    notes: [...records],
  });
};

module.exports = {
  saveNote,
  getNoteById,
  checkNoteById,
  deleteNoteById,
  updateNoteById,
  getAllUsersNote,
};
