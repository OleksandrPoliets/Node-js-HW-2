const Joi = require('joi');

const validateUserFild = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

const passwordValidation = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  const {oldPassword, newPassword} = req.body;
  await schema.validateAsync({oldPassword, newPassword});
  next();
};

const notesField = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .min(1)
        .required(),
  });

  const {text} = req.body;
  await schema.validateAsync({text});
  next();
};

module.exports = {validateUserFild, passwordValidation, notesField};
