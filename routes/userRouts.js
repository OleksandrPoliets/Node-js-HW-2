const {Router} = require('express');
const {
  authorization,
  getUserData,
  changePassword,
  deleteUser,
} = require('../middleware/authMiddleware');
const {asyncWrapper} = require('../helpers/asyncWraper');
const {passwordValidation} = require('../middleware/validateUserFild');
const router = new Router();

router.get('/me', asyncWrapper(authorization), asyncWrapper(getUserData))
    .delete('/me', asyncWrapper(authorization), asyncWrapper(deleteUser))
    .patch('/me',
        asyncWrapper(authorization),
        asyncWrapper(passwordValidation),
        asyncWrapper(changePassword));


module.exports = router;
