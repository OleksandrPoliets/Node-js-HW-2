const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const userRouts = require('./userRouts');
const notesRouts = require('./notesRoutes');
const authRouts = require('./authRouts');

const accessLogStream = fs.createWriteStream(
    path.join(__dirname, '../', 'server.log'),
    {flags: 'a'},
);

module.exports = (app) => {
  app.use(cors());
  app.use(express.json());
  app.use(morgan('combined', {stream: accessLogStream}));
  app.use('/api/users', userRouts);
  app.use('/api/notes', notesRouts);
  app.use('/api/auth', authRouts);
  app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
      return res.status(400).json({message: err.message});
    }
    if (err.name === 'JsonWebTokenError') {
      return res.status(400).json({message: err.message});
    }
    if (err.name === 'CastError') {
      return res.status(400).json({message: 'wrong note id'});
    }
    return res.status(500).json({message: err.message});
  });
};
