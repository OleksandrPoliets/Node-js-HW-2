const {Router} = require('express');
const {authorization} = require('../middleware/authMiddleware');
const {asyncWrapper} = require('../helpers/asyncWraper');
const {notesField} = require('../middleware/validateUserFild');
const {saveNote,
  getNoteById,
  checkNoteById,
  deleteNoteById,
  updateNoteById,
  getAllUsersNote,
} = require('../middleware/notesMiddleware');
const router = new Router();

router.get('/', asyncWrapper(authorization), asyncWrapper(getAllUsersNote))
    .get('/:id', asyncWrapper(authorization), asyncWrapper(getNoteById))
    .put('/:id',
        asyncWrapper(authorization),
        asyncWrapper(notesField),
        asyncWrapper(updateNoteById))
    .post('/',
        asyncWrapper(authorization),
        asyncWrapper(notesField),
        asyncWrapper(saveNote))
    .patch('/:id', asyncWrapper(authorization), asyncWrapper(checkNoteById))
    .delete('/:id', asyncWrapper(authorization), asyncWrapper(deleteNoteById));


module.exports = router;
