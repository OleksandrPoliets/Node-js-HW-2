const {Router} = require('express');
const {validateUserFild} = require('../middleware/validateUserFild');
const {asyncWrapper} = require('../helpers/asyncWraper');
const {
  checkUserName,
  registerUser,
  loginUser,
} = require('../middleware/authMiddleware');

const router = new Router();

router.post('/register',
    asyncWrapper(validateUserFild),
    asyncWrapper(checkUserName),
    asyncWrapper(registerUser))
    .post('/login', asyncWrapper(validateUserFild), asyncWrapper(loginUser));


module.exports = router;
