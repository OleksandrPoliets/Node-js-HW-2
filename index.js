require('dotenv').config();
const mongoose = require('mongoose');
const express = require('express');
const routes = require('./routes/index');

const app = express();
const PORT = process.env.PORT;

routes(app);

const startServer = async () => {
  await mongoose.connect('mongodb+srv://testuser:qwerty123@cluster0.piahq.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(process.env.PORT, () => {
    console.log(`Server listening on port ${PORT}!`);
  });
};

startServer();
